package mongodbevent

import (
	"context"
	"log"
	"time"
	"errors"
 
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive" // for BSON ObjectID
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
 )

var databaseName string = "rookieTrainerDb"
var collectionUser string = "user"

 type UserJson struct {
	Id interface {} `bson:"_id,omitempty"`
	Name string `bson:"name"`
	AvatarType string `bson:"avatar_type"`
	AvatarName string `bson:"avatar_name"`
	Age int `bson:"age"`
	YearOfBirth int `bson:"year_of_birth"`
	Note string `bson:"note,omitempty"`
	Email string `bson:"email"`
	CreatedAt time.Time `bson:"created_at"`
	UpdatedAt time.Time `bson:"updated_at"`
}

func connectMongoDB() (*mongo.Client, error) {
	mongodbUrl := "mongodb://localhost:27017"

	// Set client options
	clientOptions := options.Client().ApplyURI(mongodbUrl)
		
	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	
	// Check the connection
	err = client.Ping(context.TODO(), nil)
	
	return client, err
}

func disconnectMongoDB(client *mongo.Client) error {

	err := client.Disconnect(context.TODO())
	return err
}

func InsertUser(userData *UserJson) (interface {}, error) {

	// Set create and update date
	userData.CreatedAt = time.Now()
	userData.UpdatedAt = userData.CreatedAt

	client, err := connectMongoDB()

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	// Select collection to connecting
	userCollection := client.Database(databaseName).Collection(collectionUser)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	// Insert data to database
	res, err := userCollection.InsertOne(ctx, userData)
	if err != nil { log.Fatal(err) }
	// Get inserted ID back
	id := res.InsertedID

	// Disconnect to MongoDB
	err = disconnectMongoDB(client)
	if err != nil {
		log.Fatal(err)
	}

	return id, err

}

func IsUserEmailHave(emailName string) (bool, error) {

	userData := new(UserJson)
	emailHave := false

	// Connect to MongoDB
	client, err := connectMongoDB()

	if err != nil {
		return emailHave, err
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		return emailHave, err
	}

	// Select collection to connecting
	userCollection := client.Database(databaseName).Collection(collectionUser)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	filter := bson.M{"email": emailName} // Filter for selecting data from database follow email

	// Query data from database
	err = userCollection.FindOne(ctx, filter).Decode(&userData)
	if err != nil {
		if err.Error() != "mongo: no documents in result" {
			return emailHave, err
		}
	}

	// Disconnect to MongoDB
	err = disconnectMongoDB(client)
	if err != nil {
		return emailHave, err
	}

	// Check request email has in database
	if userData.Email == emailName {
		emailHave = true
	}

	return emailHave, err
}

func GetUserById(userId string) (*UserJson, error) {

	userData := new(UserJson)

	// Connect to MongoDB
	client, err := connectMongoDB()

	if err != nil {
		return userData, err
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		return userData, err
	}

	// Select collection to connecting
	userCollection := client.Database(databaseName).Collection(collectionUser)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	// Change userID format from string to ObjectID
	docID, err := primitive.ObjectIDFromHex(userId)
	if err != nil {
		err = errors.New("Fomat is incorrect for objID")
		return userData, err
	}

	filter := bson.M{"_id": docID} // Filter for selecting data from database follow userID

	// Query data from database
	err = userCollection.FindOne(ctx, filter).Decode(&userData)
	if err != nil {
		return userData, err
	}

	// Disconnect to MongoDB
	err = disconnectMongoDB(client)
	if err != nil {
		return userData, err
	}

	return userData, err
}

func UpdateUserById(userId string, userData *UserJson) (error) {

	var mapData = make(map[string]interface {})

	userData.UpdatedAt = time.Now()

	if userData.Name != "" {
		mapData["name"] = userData.Name
	}
	if userData.AvatarType != "" {
		mapData["avatar_type"] = userData.AvatarType
		mapData["avatar_name"] = userData.AvatarName
	}
	if userData.Age != 0 {
		mapData["age"] = userData.Age
		mapData["year_of_birth"] = userData.YearOfBirth
	}
	if userData.Note != "" {
		mapData["note"] = userData.Note
	}
	if len(mapData) > 0 {
		mapData["updated_at"] = userData.UpdatedAt
	}

	// Connect to MongoDB
	client, err := connectMongoDB()

	if err != nil {
		return err
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		return err
	}

	// Select collection to connecting
	userCollection := client.Database(databaseName).Collection(collectionUser)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	// Change userID format from string to ObjectID
	docID, err := primitive.ObjectIDFromHex(userId)
	if err != nil {
		err = errors.New("Fomat is incorrect for objID")
		return err
	}


	filter := bson.M{"_id": docID} // Filter for selecting data from database follow userID

	// Update data to database
	_, err = userCollection.UpdateOne(
		ctx,
		filter,
		bson.D {
			{"$set", mapData},
		},
	)
	if err != nil {
		return err
	}

	// Special case if "clean" is a update data for "note" field, set this field to void
	if userData.Note == "clean" {
		_, err = userCollection.UpdateOne(
			ctx,
			filter,
			bson.D {
				{"$unset", bson.M{"note": ""}},
			},
		)
		if err != nil {
			return err
		}
	}

	// Disconnect to MongoDB
	err = disconnectMongoDB(client)
	if err != nil {
		return err
	}

	return err
}

func GetUserLimitPage(limit int, page int) ([]UserJson, int, error) {
	var userListData []UserJson
	userDataCount := 0
	var userPageData []UserJson
	lowest_count_data := (limit * (page-1)) + 1

	// Connect to MongoDB
	client, err := connectMongoDB()

	if err != nil {
		return userPageData, userDataCount, err
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		return userPageData, userDataCount, err
	}

	// Select collection to connecting
	userCollection := client.Database(databaseName).Collection(collectionUser)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	filter := bson.M{}	// Filter for selecting data from database

	// Options for ascending sort data by created date
	opts := options.Find()
	opts.SetSort(bson.D{{"created_at", 1}})

	// Query database
	cursor, err := userCollection.Find(ctx, filter, opts)
	if err = cursor.All(ctx, &userListData); err != nil {
		return userPageData, userDataCount, err
	}

	// Disconnect to MongoDB
	err = disconnectMongoDB(client)
	if err != nil {
		return userPageData, userDataCount, err
	}

	// Get amout of userData
	userDataCount = len(userListData)

	// Check amout of userData can show follow request
	if userDataCount < lowest_count_data{
		err := errors.New("Number of Userdata not enough to show")
		return userPageData, userDataCount, err
	}

	// Create min and max index for slice userData to show follow page
	pageMinIndex := (page - 1) * limit
	pageMaxIndex := (page * limit)

	//  Set case last page show only userData have
	if pageMaxIndex > userDataCount {
		pageMaxIndex = userDataCount
	}

	// Slice userData follow selecting page
	userPageData = userListData[pageMinIndex:pageMaxIndex]

	return userPageData, userDataCount, err
}

func DeleteUserById(userId string) (interface {}, error) {
	deleteSuccess := bson.M{"success": false}

	// Connect to MongoDB
	client, err := connectMongoDB()

	if err != nil {
		return deleteSuccess, err
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		return deleteSuccess, err
	}

	// Select collection to connecting
	userCollection := client.Database(databaseName).Collection(collectionUser)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	// Change userID format from string to ObjectID
	docID, err := primitive.ObjectIDFromHex(userId)
	if err != nil {
		err = errors.New("Fomat is incorrect for objID")
		return deleteSuccess, err
	}
	
	filter := bson.M{"_id": docID} // Filter for selecting data from database follow userID

	// Delete data from database
	result, err := userCollection.DeleteOne(ctx, filter)
	if err != nil {
		return deleteSuccess, err
	}

	// Disconnect to MongoDB
	err = disconnectMongoDB(client)
	if err != nil {
		return deleteSuccess, err
	}

	if result.DeletedCount > 0 {
		deleteSuccess = bson.M{"success": true}
	} else {
		deleteSuccess = bson.M{"success": false}
		err = errors.New("mongo: no documents in result")
	}

	return deleteSuccess, err
}