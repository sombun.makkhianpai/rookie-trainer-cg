package main

import (
	"net/http"
	"regexp"
	"time"
	"strconv"
	"path/filepath"

	"github.com/labstack/echo/v4"
	"gitlab.com/sombun.makkhianpai/rookie-trainer-cg/mongodbevent"
	"go.mongodb.org/mongo-driver/bson"
)

type errorMessage struct {
	Error string `bson:"error"`
}

func main() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	// Routing REST API
	e.POST("/user", saveUser)
	e.PUT("/user/:user_id", updateUser)
	e.GET("/users", showListUsers)
	e.GET("/user/:user_id", showUser)
	e.DELETE("/user/:user_id", deleteUser)

	e.Logger.Fatal(e.Start(":1323"))
}

func isEmailValid(emailName string) bool {
	emailRegex := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	if len(emailName) < 3 && len(emailName) > 254 {
		return false
	}
	return emailRegex.MatchString(emailName)
}

// e.POST("/user", saveUser)
func saveUser(c echo.Context) error {
	userData := new(mongodbevent.UserJson)
	errorJson := new(errorMessage)
	// var userID interface{}

	// Get user informations by Form
	// Get Name
	if userData.Name = c.FormValue("name"); len(userData.Name) == 0 {
		errorJson.Error = "Please enter Name"
		return c.JSON(500, errorJson)
	}

	// Get Avatar
	avatar, err := c.FormFile("avatar")
	if err != nil {
		if err.Error() == "http: no such file" {
			errorJson.Error = err.Error() + ", Please upload file"
			return c.JSON(500, errorJson)
		}
		errorJson.Error = err.Error()
		return c.JSON(500, errorJson)
	} else {
		fileName := avatar.Filename
		userData.AvatarType = filepath.Ext(fileName)[1:]
		userData.AvatarName = fileName[0:len(fileName) - len(userData.AvatarType) - 1]
	}

	// Get Age
	if age := c.FormValue("age"); len(age) == 0 {
		errorJson.Error = "Please enter Age"
		return c.JSON(500, errorJson)
	}
	userData.Age, err = strconv.Atoi(c.FormValue("age"))
	if err != nil {
		errorJson.Error = "Age must be positive number"
		return c.JSON(500, errorJson)
	}
	if userData.Age > 0 {
		userData.YearOfBirth = time.Now().Year() - userData.Age
	}

	// Get Note
	if note := c.FormValue("note"); len(note) != 0 {
		userData.Note = c.FormValue("note")
	}

	// Get Email
	if userData.Email = c.FormValue("email"); len(userData.Email) == 0 {
		errorJson.Error = "Please enter Email"
		return c.JSON(500, errorJson)
	}

	// Check user information are conditionally
	// Check Avatar file type is "jpg" or "png"
	if userData.AvatarType != "jpg" && userData.AvatarType != "png" {
		errorJson.Error = "Image type must be \"jpg\" or \"png\" only"
		return c.JSON(401, errorJson)
	}

	// Check age must be between 1- 100
	if userData.Age < 1 || userData.Age > 100 {
		errorJson.Error = "Age must be between 1 - 100"
		return c.JSON(401, errorJson)
	}

	// Check email form is correct
	if !isEmailValid(userData.Email) {
		errorJson.Error = "Email format is incorrect"
		return c.JSON(401, errorJson)
	}
	
	// Check email not dupicate in database
	emailHave, err := mongodbevent.IsUserEmailHave(userData.Email);
	if err != nil {
		errorJson.Error = err.Error()
		return c.JSON(500, errorJson)
	}
	if emailHave {
		errorJson.Error = "This Email already exists"
		return c.JSON(401, errorJson)
	}

	// Insert userData to database
	userData.Id , err = mongodbevent.InsertUser(userData)
	if err != nil {
		errorJson.Error = err.Error()
		return c.JSON(500, errorJson)
	}

	return c.JSON(201, userData)
}

// e.GET("/user/:user_id", showUser)
func showUser(c echo.Context) error {
	userData := new(mongodbevent.UserJson)
	errorJson := new(errorMessage)
	var err error

	userId := c.Param("user_id")

	userData, err = mongodbevent.GetUserById(userId)
	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			errorJson.Error = err.Error()+", Not found user_id: ["+userId+"] in database."
			return c.JSON(404, errorJson)
		}
		if err.Error() == "Fomat is incorrect for objID" {
			errorJson.Error = err.Error()+", Not found user_id: ["+userId+"] in database."
			return c.JSON(404, errorJson)
		}
		errorJson.Error = err.Error()
		return c.JSON(500, errorJson)
	}

  return c.JSON(200, userData)
}

// e.PUT("/user/:user_id", updateUser)
func updateUser(c echo.Context) error {
	userData := new(mongodbevent.UserJson)	//for data from request
	userDBData := new(mongodbevent.UserJson) //for data from database
	errorJson := new(errorMessage)
	var err error

	// Get userId by Path
	userId := c.Param("user_id")

	// Get user informations by Form
	// Get Name
	userData.Name = c.FormValue("name")

	// Get Avatar img file
	avatar, err := c.FormFile("avatar")
	if err != nil {
		if err.Error() != "http: no such file" {
			errorJson.Error = err.Error()
			return c.JSON(500, errorJson)
		}
	} else {
		fileName := avatar.Filename
		userData.AvatarType = filepath.Ext(fileName)[1:]
		userData.AvatarName = fileName[0:len(fileName) - len(userData.AvatarType) - 1]
	}

	// Get Age
	age := c.FormValue("age")
	if len(age) != 0{
		userData.Age, err = strconv.Atoi(age)
		if err != nil {
			errorJson.Error = "Age must be positive number"
			return c.JSON(500, errorJson)
		}
	}
	
	if userData.Age > 0 {
		userData.YearOfBirth = time.Now().Year() - userData.Age
	}

	// Get Note
	userData.Note = c.FormValue("note")

	// Check user information are conditionally
	// Check Avatar file type is "jpg" or "png"
	if len(userData.AvatarType) != 0 && (userData.AvatarType != "jpg" && userData.AvatarType != "png") {
		errorJson.Error = "Image type must be \"jpg\" or \"png\" only"
		return c.JSON(401, errorJson)
	}
	// Check Age must be between 1 - 100
	if userData.Age != 0 && (userData.Age < 1 || userData.Age > 100) {
		errorJson.Error = "Age must be between 1 - 100"
		return c.JSON(401, errorJson)
	}

	// Check have userID
	userDBData, err = mongodbevent.GetUserById(userId)
	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			errorJson.Error = err.Error()+", Not found user_id: ["+userId+"] in database."
			return c.JSON(401, errorJson)
		}
		if err.Error() == "Fomat is incorrect for objID" {
			errorJson.Error = err.Error()+", Not found user_id: ["+userId+"] in database."
			return c.JSON(401, errorJson)
		}
		errorJson.Error = err.Error()
		return c.JSON(500, errorJson)
	}

	// Update data to database
	err = mongodbevent.UpdateUserById(userId, userData)

	if err != nil {
		errorJson.Error = err.Error()
		return c.JSON(500, errorJson)
	}

	// Get data from database to show
	userDBData, err = mongodbevent.GetUserById(userId)
	if err != nil {
		errorJson.Error = err.Error()
		return c.JSON(500, errorJson)
	}

  return c.JSON(200, userDBData)
}

//e.GET("/users", showListUsers)
func showListUsers(c echo.Context) error {
	userAllData := new([]mongodbevent.UserJson)	//user data for page request
	var userDataCount int //kepp count all userData from database
	errorJson := new(errorMessage)

	// default data case user not enter value
	limit := 10
	page := 1
	var err error

	// Get limit and page from the query string
	if limitData := c.QueryParam("limit"); limitData != "" {
		limit, err = strconv.Atoi(limitData)
		if err != nil {
			errorJson.Error = err.Error()
			return c.JSON(500, errorJson)
		}
	}

	if pageData := c.QueryParam("page"); pageData != "" {
		page, err = strconv.Atoi(pageData)
		if err != nil {
			errorJson.Error = err.Error()
			return c.JSON(500, errorJson)
		}
	}

	if limit < 1 {
		errorJson.Error = "limit must be positive number!!! (1, 2, 3 ,....)"
		return c.JSON(500, errorJson)
	}

	if page < 1 {
		errorJson.Error = "Page must be positive number!!! (1, 2, 3 ,....)"
		return c.JSON(500, errorJson)
	}
	
	// Get data from database to show
	*userAllData, userDataCount, err = mongodbevent.GetUserLimitPage(limit, page)
	if err != nil {
		errorJson.Error = err.Error()
		return c.JSON(500, errorJson)
	}

	// set data follow format to show [count= all data form database], [data= data to show follow limit and page request]
	userDataPage := bson.M{
		"count": userDataCount,
		"data": userAllData,
	}

	return c.JSON(200, userDataPage)
}

//e.DELETE("/user/:user_id", deleteUser)
func deleteUser(c echo.Context) error {
	errorJson := new(errorMessage)

	// Get userId by Path
	userId := c.Param("user_id")

	// Delete select data by ID
	deleteResult, err := mongodbevent.DeleteUserById(userId)
	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			errorJson.Error = err.Error()+", Not found user_id: ["+userId+"] in database."
			return c.JSON(404, errorJson)
		} 
		if err.Error() == "Fomat is incorrect for objID" {
			errorJson.Error = err.Error()+", Not found user_id: ["+userId+"] in database."
			return c.JSON(404, errorJson)
		}
		errorJson.Error = err.Error()
		return c.JSON(500, errorJson)
	}

	return c.JSON(200, deleteResult)
}



