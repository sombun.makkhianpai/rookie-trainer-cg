module gitlab.com/sombun.makkhianpai/rookie-trainer-cg

go 1.15

require (
	github.com/labstack/echo/v4 v4.1.17
	go.mongodb.org/mongo-driver v1.4.6
)
